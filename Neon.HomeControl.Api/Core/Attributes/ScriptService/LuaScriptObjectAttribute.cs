﻿using System;

namespace Neon.HomeControl.Api.Core.Attributes.ScriptService
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class LuaScriptObjectAttribute : Attribute
	{
	}
}